#!/bin/bash

GRACEDAYS=60
today=`date '+%s'`

ret=0
cd src
for cert in *.crt; do
  serial=`openssl x509 -in $cert -inform DER -serial -noout | sed -e 's#serial=##'`
  startdate=`openssl x509 -in $cert -inform DER -startdate -noout | sed -e 's#notBefore=##'`
  enddate=`openssl x509 -in $cert -inform DER -enddate -noout | sed -e 's#notAfter=##'`

  certdate=`date -d "${enddate}" '+%s'`
  diff="$((${certdate}-${today}))"

  echo -n "${cert} ($serial): starts on ${startdate} and "
  if [[ "${diff}" -lt "$((${GRACEDAYS}*24*3600))" ]]; then
    ret=$(($ret+1))
    if [[ "${diff}" -lt "0" ]]; then
      echo -e "\e[1m\e[31mexpired on ${enddate}!\e[0m"
    else
      echo -e "\e[1m\e[31mwill expire in $((${diff}/3600/24)) days, on ${enddate}.\e[0m"
    fi
  else
    echo "expires on ${enddate}."
  fi
done

exit $ret
