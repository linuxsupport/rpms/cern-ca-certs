Summary: CERN CA certificates for use with system libraries
Name: CERN-CA-certs
Version: 20230604
Release: 1%{?dist}
URL: http://cern.ch/ca
Packager: Linux.Support@cern.ch
Source: %{name}-%{version}.tgz
License: distributable
Group: System/Security
BuildRoot: %{_tmppath}/%{name}-root
BuildRequires: openssl
BuildArch: noarch
Requires: coreutils
Requires: grep
Requires: ca-certificates
Requires: openldap

%description
Certificates of CERN Certificate authority (https://cern.ch/ca)
shipped as .crt .bundle and .pem and also imported to system java
certificate store(s) for all supported java versions.

Install this if you would like openssl (and other commands that use
openssl libraries) to trust certificates signed by this CA.


%prep
%setup -q

%build
%if 0%{?rhel} >= 6
make all USE_HASH_OLD=y
%else
make all USE_HASH_OLD=n
%endif
make test

%install
make install DESTDIR=$RPM_BUILD_ROOT prefix=%{_prefix}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
/etc/pki/tls/certs/*
/etc/openldap/cacerts
/etc/openldap/cacerts/*
/usr/share/pki/ca-trust-source/anchors/CERN*.crt

%posttrans
if [ -x /usr/bin/update-ca-trust ]; then
  echo "Updating certificates for CERN-CA-certs"
  if [ `/usr/bin/update-ca-trust check | /bin/grep -c DISABLED` -eq 1 ]; then
    # we need force to overwrite /etc/pki/java/cacerts on SLC6
    /usr/bin/update-ca-trust force-enable || :
  fi
  /usr/bin/update-ca-trust extract || :
fi
:

%changelog
* Sun Jun 04 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 20230604-1
- Removed CERN_Certification_Authority and CERN_Certification_Authority(1)

* Wed Apr 26 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 20230421-2
- Run update-ca-trust extract on posttrans so we pick up only the new certificates
- Drop legacy cern-import-certs-java.sh
- Take ownership of /etc/openldap/cacerts and declare dependency on openldap

* Fri Apr 21 2023 Alex Iribarren <Alex.Iribarren@cern.ch> - 20230421-1
- Removed CERN_Grid_Certification_Authority, expires on Apr 22 11:20:16 2023 GMT

* Tue Mar 29 2022 Ben Morrice <ben.morrice@cern.ch> 20220329-1
- added new CERN CA certificates (retaining old ones for now)
- removed IPAdev root certificate

* Fri Jun 25 2021 Alex Iribarren <Alex.Iribarren@cern.ch> 20200530-2
- Added SHA256 hashes (thanks jcastro)

* Sat May 30 2020 Julien Rische <julien.rische@cern.ch> 20200530-1
- remove expired AddTrustExternalCARoot and
  VeriSignClass3SecureServerCA-G3

* Mon Apr 20 2020 Julien Rische <julien.rische@cern.ch> 20200420-1
- add root certificate of IPAdev CERN CA

* Wed May 16 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20180516-1
- add missing CERN_Certification_Authority(1).crt to java cert store.

* Tue Mar 13 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20180313-1
- removed expired CERN_Trusted_Certification_Authority

* Mon Oct 09 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> 2017100901
- added new CERN_Certification_Authority(1).crt/pem

* Thu May 19 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20160421-2
- added missing coreutils, grep rpm dependency

* Thu Apr 21 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20160421-1
- enable consolidated dynamic configuration of CA certificates
  on SLC6 too.

* Tue Jul 21 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20150721-1
- add consolidated and dynamic configuration of CA certificates
  using update-ca-trust (SLC6, CC7)

* Wed Feb 18 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20150218-1
- adding AddTrust External CA Root for owncloud client

* Fri Oct 24 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20140325-3
- adding triggers for java-1.8.0-{oracle,openjdk}

* Tue Mar 25 2014 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20140325-1
- Add CERN Certification Authority certificate.

* Thu Nov 21 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20130627-4
- silence cern-import-certs-java

* Fri Nov  8 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20130627-3
- added cern-import-certs-java
* Fri Jul  5 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20130627-2
- added cert hashes to /etc/openldap/cacerts/
* Thu Jun 27 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20130627-1
- added certs for new CERN CA ('CERN CA 2')

* Thu Apr  4 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20120322-10
- only *.pem certificates in /etc/openldap/cacerts/ to avoid
  authconfig errors.
* Tue Nov 27 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20120322-8
- added VeriSign Class 3 Secure Server CA - G3.

* Tue Jul 24 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20120322-7
- added 'old style hashes' (pre opensssl 1.0)

* Fri Jun 22 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch> 20120622-1
- Added certificcates for openldap
* Fri Jun 22 2012 Thomas Oulevey <thomas.oulevey@cern.ch>
- Fixes for .0 files than were not included in previous package.

* Thu Mar 22 2012 Jaroslaw Polok <jaroslaw.polok@cern.ch>
* Tue Feb 16 2010 Jan van Eldik <Jan.van.Eldik@cern.ch>
- Unify builds on SLC4 and SLC5

* Fri May  4 2007 Jan Iven  <jan.iven@cern.ch> -20061003.1
- Initial build.

